# Javascript Experiments

A collection of various mini-projects/tinkering with Javascript.


Ideas for Scripts:

*  Something that lets the user know when the lobby they made is less filled/ newer than another lobby
*  Chat message counter
*  Improved chat experience
*  Advanced autokicker (new user detection)
*  Greet script that gives more advanced greetings (e.g. if it has been awhile, greet with an appropriate "long time no see!" - aquarius)
*  Move lobby chat box to right, keep it open, people will use it more if it is visible.
*  Mute function (hides users messages!!!)